CC = nim
FILES = src/narc.nim
OUT_EXE = src/narc
LIBS = d:ssl
PREFIX ?= /usr

all:
	@echo Run \'make build\' to complie NARC.
	@echo Run \'make clean\' to remove previously comiled build of NARC.
	@echo Run \'make install\' to install NARC.
	@echo Run \'make uninstall\' to uninstall NARC.

build: $(FILES)
	$(CC) c -$(LIBS) $(FILES) 

clean:
	rm -f *.o core
	rm -f $(OUT_EXE)

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p $(OUT_EXE) $(DESTDIR)$(PREFIX)/bin/narc
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/narc

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/narc
