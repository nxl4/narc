```
888b    888        d8888 8888888b.   .d8888b.  
8888b   888       d88888 888   Y88b d88P  Y88b 
88888b  888      d88P888 888    888 888    888 
888Y88b 888     d88P 888 888   d88P 888        
888 Y88b888    d88P  888 8888888P"  888        
888  Y88888   d88P   888 888 T88b   888    888 
888   Y8888  d8888888888 888  T88b  Y88b  d88P 
888    Y888 d88P     888 888   T88b  "Y8888P"  
                                               
                                  
NARC, version 0.1
```

## General Description

The Nim-Adapted RDAP Client (NARC), is a client application for the regional internet registry (RIR) registration data access protocol (RDAP) information written in [Nim](https://nim-lang.org/). The client provides both human-readable and `JSON` formatted RDAP responses to queries for IP addresses, CIDR ranges, ASNs, and domain names. RDAP data for IP addresses, CIDR ranges, and ASNs is fetched directly from the RIRS, which provide coverage for all valid IPv4 and IPv6 addresses and ASNs	:

* [American Registry for Internet Numbers (ARIN)](https://www.arin.net/)
* [Réseaux IP Européens Network Coordination Centre (RIPE NCC)](https://www.ripe.net/)
* [Asia-Pacific Network Information Centre (APNIC)](https://www.apnic.net/)
* [Latin America and Caribbean Network Information Centre (LACNIC)](https://www.lacnic.net/)
* [African Network Information Center (AFRINIC)](https://www.afrinic.net/)

RDAP data for domain names is retrieved from [Verisign](https://www.verisign.com/), which includes RDAP data for a subset of major top-level domains (TLDs) --- .com, .net, .name, .cc and .tv domain names --- as well as several internationalized domain names (IDNs) such as .コム, .닷컴, .닷넷 and .كوم.

## Input

### Positional Arguments

The application accepts a single positional argument, consisting of either an IP address or domain name.

### Optional Arguments

There are four optional arguments:

|Short Form|Long Form|Description                          |
|----------|---------|-------------------------------------|
|-h        |--help   |Show the help Message and Exits      |
|-i        |--input  |Defines Search Term Type             |
|-o        |--output |Define Output Type                   |
|-p        |--pretty |Pretty Prints `JSON` Formatted Output|

For the `input` argument, there are four valid options:

1. `ip`, used to query IP addresses
2. `cidr`, used to query CIDR ranges
3. `asn`, used to query ASNs (the "AS-" prefix is optional)
4. `domain`, used to query domain names

When no `input` argument is specified, the application will attempt (with a relatively high degree of accuracy) to infer the input type. For the `output` argument, there are two valid options:

1. `line`, outputs human-readable newline responses
2. `json`, outputs `JSON` formatted responses

When no `output` argument is specified, the application defaults to `line`. The output formatting applies equally to successful query results as well as all error messaging.

## Usage

The general usage paradigm is:

```shell
narc [-h][-p][-i {ip, cidr, asn, domain} | -o {line, json}] search_term
```
            
### Examples

A simple query for an IP address, with result returned in line format can be executed as follow:

```shell
narc 8.8.8.8		
```


A search specifying the input as an ASN and the output as pretty-printed `JSON` can be structured as follows:

```shell
narc -i:asn -o:json -p AS1234
```

A search specifying the input as a domain, and the output as line format can be structures as follows:

```shell
narc -i:domain -o:line example.com
```

## Output

#### Successful Responses

A line-printed example of an IP address query:

![example_01](img/example_01.png)

A line-printed example of a domain query:

![example_02](img/example_02.png)

A pretty-printed example of a JSON-formatted IP address query:

![example_03](img/example_03.png	)

#### Error Responses

A line-printed error message:

![example_04](img/example_04.png)

A JSON-formatted error message:

![example_05](img/example_05.png)	

## Installation

### Requirements

To compile the program, [Nim](https://nim-lang.org/install.html) is required.

### UNIX-like

The easiest way to install this utility is to first clone the repository:

```shell
git clone https://gitlab.com/nxl4/narc.git
```

Then, run the `make` command on the `makefile` from within the cloned repository's new directory to *first* compile the program:

```shell
make clean build
```

And once the program has been compiled, it can be installed for general use throughout the system:

```shell
sudo make install
```

Likewise, the `make` command can be used to uninstall the utility:

```shell
sudo make uninstall
```

## Issues

Please report any issues to the [Issue Tracker](https://gitlab.com/nxl4/narc/issues/new) for this repository. 


## Author

nxl4: [nxl4@protonmail.com](nxl4@protonmail.com)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)