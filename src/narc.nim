include modules/[
    argParsers,
    inputClassifiers,
    fancyWriters,
    inputHandlers,
    helpWriter
]

proc main() =
    let args = getCliArgs()
    let eval = evalParsedArgs(args)
    let final = classifyEvaluation(eval)
    if final.input_valid == false:
        handleErrors(
            final.input_value,
            final.errors, 
            final.output_type, 
            final.pretty
        )
    elif final.input_valid == true:
        if final.input_type == "help":
            writeHelp()
        elif final.input_type == "ip":
            handleRdapRequests(
                "ip",
                final.input_value,
                final.output_type, 
                final.pretty
            )
        elif final.input_type == "cidr":
            handleRdapRequests(
                "cidr",
                final.input_value,
                final.output_type,
                final.pretty
            )
        elif final.input_type == "asn":
            handleRdapRequests(
                "asn",
                final.input_value,
                final.output_type,
                final.pretty
            )
        elif final.input_type == "domain":
            handleRdapRequests(
                "domain",
                final.input_value,
                final.output_type,
                final.pretty
            )


main()
