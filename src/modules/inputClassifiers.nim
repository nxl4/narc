import net
import re
import strutils

type
    IpClassification = object
        ip_value: string
        ip_valid: bool
        ip_version: string

    CidrClassification = object
        cidr_value: string
        cidr_valid: bool
        cidr_version: string

    DomainClassification = object
        domain_value: string
        domain_valid: bool

    AsnClassification = object
        asn_value: string
        asn_valid: bool

    GenericClassification = object
        value: string
        valid_type: bool
        inferred_type: string
        ip_rsp: IpClassification
        cidr_rsp: CidrClassification
        domain_rsp: DomainClassification
        asn_rsp: AsnClassification

    ClassifiedEvaluation = object
        input_value: string
        input_type: string
        input_valid: bool
        output_type: string
        errors: seq[string]
        pretty: bool
        

var ipv4_regex = re"(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}"

var domain_regex = re"^([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,}$"

var asn_regex = re"^[0-9]{1,10}$"

proc classifyIp(value: string): IpClassification = 
    var rsp: IpClassification
    rsp.ip_value = value
    let is_ip = isIpAddress(value)
    if is_ip == true:
        rsp.ip_valid = true
        let ipv4 = findAll(value, ipv4_regex)
        if ipv4.len() == 1:
            rsp.ip_version = "IPv4"
        else:
            rsp.ip_version = "IPv6"
    else:
        rsp.ip_valid = false
    return rsp


proc classifyCidr(value: string): CidrClassification =
    var rsp: CidrClassification
    rsp.cidr_value = value
    let ipv4_masks = [
        "0",  "1",  "2",  "3",  "4",  "5",  "6",  "7",  "8",  "9",  "10", 
        "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", 
        "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32"
    ]
    let ipv6_masks = [
        "4",   "8",  "12",  "16",  "20",  "24",  "28",  "32",  "36",  "40",  "44", 
        "48",  "52", "56",  "60",  "64",  "68",  "72",  "76",  "80",  "84",  "88", 
        "92",  "96", "100", "104", "108", "112", "116", "120", "124", "127", "128"
    ]
    let cidr_split = value.split("/")
    if cidr_split.len() == 2:
        let ip_addr = cidr_split[0]
        let subnet_mask = cidr_split[1]
        let ip_classify = classifyIp(ip_addr)
        if ip_classify.ip_valid == true:
            if ip_classify.ip_version == "IPv4":
                if subnet_mask in ipv4_masks:
                    rsp.cidr_valid = true
                    rsp.cidr_version = "IPv4"
                else:
                    rsp.cidr_valid = false
            elif ip_classify.ip_version == "IPv6":
                if subnet_mask in ipv6_masks:
                    rsp.cidr_valid = true
                    rsp.cidr_version = "IPv6"
                else:
                    rsp.cidr_valid = false
        else:
            rsp.cidr_valid = false
    else:
        rsp.cidr_valid = false
    return rsp
    

proc classifyDomain(value: string): DomainClassification =
    var rsp: DomainClassification
    rsp.domain_value = value
    let domain = findAll(value, domain_regex)
    if domain.len() == 1:
        rsp.domain_valid = true
    else:
        rsp.domain_valid = false
    return rsp


proc classifyAsn(value: string): AsnClassification = 
    var rsp: AsnClassification
    var asn_value: string
    asn_value = value.replace("as", "").replace("AS", "")
    rsp.asn_value = asn_value
    let asn = findAll(asn_value, asn_regex)
    if asn.len() == 1:
        let asn_int = parseInt(asn_value)
        if asn_int <= 4294967295:
            rsp.asn_valid = true
        else: 
            rsp.asn_valid = false
    else:
        rsp.asn_valid = false
    return rsp


proc classifyGeneric(value: string): GenericClassification =
    var rsp: GenericClassification
    rsp.value = value
    let ip_rsp = classifyIp(value)
    rsp.ip_rsp = ip_rsp
    let cidr_rsp = classifyCidr(value)
    rsp.cidr_rsp = cidr_rsp
    let domain_rsp = classifyDomain(value)
    rsp.domain_rsp = domain_rsp
    let asn_rsp = classifyAsn(value)
    rsp.asn_rsp = asn_rsp
    if ip_rsp.ip_valid == true:
        rsp.valid_type = true
        rsp.inferred_type = "ip"
    elif cidr_rsp.cidr_valid == true:
        rsp.valid_type = true
        rsp.inferred_type = "cidr"
    elif domain_rsp.domain_valid == true:
        rsp.valid_type = true
        rsp.inferred_type = "domain"
    elif asn_rsp.asn_valid == true:
        rsp.value = asn_rsp.asn_value
        rsp.valid_type = true
        rsp.inferred_type = "asn"
    else:
        rsp.valid_type = false
    return rsp


proc classifyEvaluation(eval: EvaluatedArgs): ClassifiedEvaluation =
    var rsp: ClassifiedEvaluation
    if eval.eval_result == "help":
        rsp.input_valid = true
        rsp.input_type = "help"
    elif eval.eval_result == "valid":
        rsp.output_type = eval.output_type
        rsp.pretty = eval.pretty
        if eval.input_type == "implied":
            let classified_input = classifyGeneric(eval.argument)
            rsp.input_value = classified_input.value
            rsp.input_valid = classified_input.valid_type
            rsp.input_type = classified_input.inferred_type
            if classified_input.valid_type == false:
                rsp.errors.add("No valid input type could be inferred")
        elif eval.input_type == "ip":
            let classified_input = classifyIp(eval.argument)
            rsp.input_value = classified_input.ip_value
            rsp.input_valid = classified_input.ip_valid
            rsp.input_type = eval.input_type
            if classified_input.ip_valid == false:
                rsp.errors.add("Invalid IP address input")
        elif eval.input_type == "cidr":
            let classified_input = classifyCidr(eval.argument)
            rsp.input_value = classified_input.cidr_value
            rsp.input_valid = classified_input.cidr_valid
            rsp.input_type = eval.input_type
            if classified_input.cidr_valid == false:
                rsp.errors.add("Invalid CIDR range input")
        elif eval.input_type == "asn":
            let classified_input = classifyAsn(eval.argument)
            rsp.input_value = classified_input.asn_value
            rsp.input_valid = classified_input.asn_valid
            rsp.input_type = eval.input_type
            if classified_input.asn_valid == false:
                rsp.errors.add("Invalid ASN input")
        elif eval.input_type == "domain":
            let classified_input = classifyDomain(eval.argument)
            rsp.input_value = classified_input.domain_value
            rsp.input_valid = classified_input.domain_valid
            rsp.input_type = eval.input_type
            if classified_input.domain_valid == false:
                rsp.errors.add("Invalid domain name input")
    elif eval.eval_result == "invalid":
        rsp.input_valid = false
        rsp.output_type = eval.output_type
        rsp.errors = eval.errors
        rsp.pretty = eval.pretty
    return rsp
