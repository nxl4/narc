import httpclient
import json

type
    RdapRsp = object
        input_value: string
        input_type: string
        error_flag: bool
        pretty_flag: bool
        rsp_body: string

const ip_urls = [
    "https://rdap.arin.net/registry/ip/", 
    "https://rdap.db.ripe.net/ip/",
    "https://rdap.apnic.net/ip/",  
    "https://rdap.lacnic.net/rdap/ip/", 
    "https://rdap.afrinic.net/rdap/ip/"
]

const asn_urls = [
    "https://rdap.arin.net/registry/autnum/",
    "https://rdap.db.ripe.net/autnum",
    "https://rdap.apnic.net/autnum/",  
    "https://rdap.lacnic.net/rdap/autnum/", 
    "https://rdap.afrinic.net/rdap/autnum/"
]

const domain_urls = [
    "https://rdap.verisign.com/com/v1/domain/"
]
# No registry RDAP server was identified for this domain. Attempting lookup using WHOIS service.
# Failed to perform lookup using WHOIS service: TLD_NOT_SUPPORTED.
# Verisign’s Registration Data Access Protocol (RDAP) service allows users to look up records in the registry database for all registered .com, .net, .name, .cc and .tv domain names. It also supports Internationalized Domain Names (IDNs) such as .コム, .닷컴, .닷넷 and .كوم.

proc handleErrors(
        input_value: string, 
        errors: seq[string], 
        output_type: string, 
        pretty: bool
    ) = 
    if output_type == "json":
        var json_rsp = %*
            {
                "input_value": input_value,
                "errors": errors
            }
        if pretty == true:
            echo pretty(json_rsp)
        else:
            echo json_rsp
    else:
        if errors.len() == 1:
            singleErrorWrite(errors[0])
        elif errors.len() > 1:
            multiErrorWrite(errors)


proc writeJson(rsp: RdapRsp) = 
    try:
        let json_body = parseJson(rsp.rsp_body)
        if rsp.pretty_flag == true:
            echo pretty(json_body)
        else:
            echo json_body
    except:
        var errors = newSeq[string]()
        errors.add("Unable to parse JSON")
        handleErrors(
            rsp.input_value,
            errors,
            "json",
            rsp.pretty_flag
        )


proc writeLines(rsp: RdapRsp) =
    try:
        #let json_body = parseJson(rsp.rsp_body)
        writeLines(rsp.input_type, rsp.input_value, rsp.rsp_body)
    except:
        var errors = newSeq[string]()
        errors.add("Unable to parse response into lines")
        handleErrors(
            rsp.input_value,
            errors,
            "line",
            rsp.pretty_flag
        )


proc handleRdapRequests(
        input_type: string, 
        input_value: string, 
        output_type: string, 
        pretty: bool
    ) =
    var rdap_rsp: RdapRsp
    rdap_rsp.input_value = input_value
    rdap_rsp.input_type = input_type
    rdap_rsp.pretty_flag = pretty
    var client = newHttpClient()
    var url: string
    var rsp: Response
    var rsp_status: string
    var query_success = false
    #var error_flag = false
    var statuses = newSeq[string]()
    var urls = newSeq[string]()
    if (input_type == "ip") or (input_type == "cidr"):
        for url in ip_urls:
            urls.add(url)
    elif input_type == "asn":
        for url in asn_urls:
            urls.add(url)
    elif input_type == "domain":
        for url in domain_urls:
            urls.add(url)
    var i = 0
    while query_success == false and i < urls.len():
        try:
            url = urls[i] & input_value
            rsp = request(client, url)
            rsp_status = rsp.status[0 .. 2]
            if rsp_status == "200":
                query_success = true
            else:
                let status = [rsp_status, " status for ", url].join()
                statuses.add(status)
        except OSError as e:
            #var error_flag = true
            rdap_rsp.rsp_body = e.msg
            rsp = nil
        inc(i)
    #rdap_rsp.error_flag = error_flag
    if query_success == true:
        rdap_rsp.rsp_body = rsp.body
    if query_success == true:
        ### TODO, differenteate between JSON and LINE outputs
        if output_type == "json":
            writeJson(rdap_rsp)
        elif output_type == "line":
            writeLines(rdap_rsp)
    else:
        var errors = newSeq[string]()
        let error_body = rdap_rsp.rsp_body.split("\n")
        for i in error_body:
            if strip(i) != "":
                errors.add(i)
        for i in statuses:
            errors.add(i)
        handleErrors(
            input_value,
            errors,
            output_type,
            pretty
        )
