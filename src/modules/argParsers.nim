import parseopt

type
    CliArgs = object
        arguments: seq[string]
        input_values: seq[string]
        output_values: seq[string]
        valueless_keys: seq[string]
        help_keys: seq[string]
        pretty_keys: seq[string]
        unrecognized_keys: seq[string]

    EvaluatedArgs = object
        help: bool
        pretty: bool
        arg_error: bool
        input_error: bool
        output_error: bool
        errors: seq[string]
        argument: string
        input_type: string
        output_type: string
        eval_result: string


proc getCliArgs(): CliArgs =
    var rsp: CliArgs
    var p = initOptParser()
    while true:
        p.next()
        case p.kind
        of cmdEnd: break
        of cmdShortOption, cmdLongOption:
            if p.val == "":
                if p.key in ["i", "input", "o", "output"]:
                    rsp.valueless_keys.add(p.key)
                elif p.key in ["h", "help"]:
                    rsp.help_keys.add(p.key)
                elif p.key in ["p", "pretty"]:
                    rsp.pretty_keys.add(p.key)
                else:
                    rsp.unrecognized_keys.add(p.key)
            else:
                if p.key in ["i", "input"]:
                    rsp.input_values.add(p.val)
                elif p.key in ["o", "output"]:
                    rsp.output_values.add(p.val)
                elif p.key in ["h", "help"]:
                    rsp.help_keys.add(p.key)
                elif p.key in ["p", "pretty"]:
                    rsp.pretty_keys.add(p.key)
                else:
                    rsp.unrecognized_keys.add(p.key)
        of cmdArgument:
            rsp.arguments.add(p.key)
    return rsp


proc evalParsedArgs(parsed_args: CliArgs): EvaluatedArgs =
    var rsp: EvaluatedArgs
    # evaluate help
    if parsed_args.help_keys.len() > 0:
        rsp.help = true
    # evaluate pretty
    if parsed_args.pretty_keys.len() > 0:
        rsp.pretty = true
    # evaluate arguments
    if parsed_args.arguments.len() > 1:
        rsp.arg_error = true
        rsp.errors.add("Too many arguments, only 1 allowed")
    elif parsed_args.arguments.len() < 1:
        rsp.arg_error = true
        rsp.errors.add("Too few arguments, 1 required")
    else:
        rsp.argument = parsed_args.arguments[0]
    # evaluate input
    if parsed_args.input_values.len() > 1:
        rsp.input_error = true
        rsp.errors.add("Too many input values, only 1 allowed")
    elif parsed_args.input_values.len() == 0:
        rsp.input_type = "implied"
    elif parsed_args.input_values.len() == 1:
        if parsed_args.input_values[0] in ["ip", "cidr", "asn", "domain"]:
            rsp.input_type = parsed_args.input_values[0]
        else:
            rsp.input_error = true
            let invalid_input = parsed_args.input_values[0]
            rsp.errors.add("Invalid input option " & invalid_input)
    # evaluate output
    if parsed_args.output_values.len() > 1:
        rsp.output_error = true
        rsp.errors.add("Too many output values, only 1 allowed")
    elif parsed_args.output_values.len() == 0:
        rsp.output_type = "line"
    elif parsed_args.output_values.len() == 1:
        if parsed_args.output_values[0] in ["line", "json"]:
            rsp.output_type = parsed_args.output_values[0]
        else:
            rsp.output_error = true
            let invalid_output = parsed_args.output_values[0]
            rsp.errors.add("Invalid output option " & invalid_output)
    # evaluate unrecognized keys
    if parsed_args.unrecognized_keys.len() > 0:
        rsp.output_error = true
        rsp.errors.add("Unrecognized option(s)")
    # holistic evaluation
    if rsp.help == true:
        rsp.eval_result = "help"
    elif true in [rsp.arg_error, rsp.input_error, rsp.output_error]:
        rsp.eval_result = "invalid"
    else:
        rsp.eval_result = "valid"
    return rsp
