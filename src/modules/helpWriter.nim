const help_text = """
Usage: narc [-h]
            [-i {ip, cidr, asn, domain} | -o {line, json}]
            search_term

NARC, 0.1 (https://gitlab.com/nxl4/narc): Nim-Adapted RDAP Client

Positional Arguments:
    search_term     IP address, Domain

Optional Arguments:
    -h, --help      show this help message and exit
    -p, --pretty    pretty print JSON output
    -i, --input     define search term type (ip, cidr, asn, domain)
    -o, --output    define output type (line, json)

Examples:
    narc 8.8.8.8
    narc example.com
    narc -i:ip -o:json -p 8.8.8.8
    narc -i:domain -o:line example.com
"""

proc writeHelp =
    echo help_text
