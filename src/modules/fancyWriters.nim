import json
import strformat
import terminal

proc singleErrorWrite(error_msg: string) = 
    styledWriteLine(
        stdout, 
        fgRed, 
        styleBright,
        "Error: ", 
        resetStyle, 
        error_msg
    )


proc multiErrorWrite(error_msg: seq[string]) =
    styledWriteLine(
        stdout,
        fgRed,
        styleBright,
        "Errors: "
    )
    for msg in error_msg:
        styledWriteLine(
            stdout,
            fgRed,
            styleBright,
            "  + ",
            resetStyle,
            msg
        )


proc infoWrite(key: string, value: string) = 
    styledWriteLine(
        stdout,
        fgCyan,
        styleBright,
        &"""{key:<15}""",
        resetStyle,
        value
    )


proc bulletWrite(value: string) =
    let width = terminalWidth()
    let val_width = width - 6
    if value.len() <= val_width:
        styledWriteLine(
            stdout,
            fgCyan,
            styleBright,
            "  + ",
            resetStyle,
            value
        )
    elif value.len() > val_width:
        var value_list = newSeq[string]()
        var i = 0
        while i < value.len() - 1:
            try:
                let end_i = i + val_width
                let slice = value[i .. end_i]
                value_list.add(slice)
                inc(i, val_width + 1)
            except IndexError:
                let slice = value[i .. value.len() - 1]
                value_list.add(slice)
                inc(i, val_width + 1)
        styledWriteLine(
            stdout,
            fgCyan,
            styleBright,
            "  + ",
            resetStyle,
            value_list[0]
        )
        var j = 1
        while j < value_list.len():
            styledWriteLine(
                stdout,
                "    " & value_list[j]
            )
            inc(j)


proc infoBulletWrite(key: string, value: string) = 
    let width = terminalWidth()
    let val_width = width - 16
    if value.len() <= val_width:
        styledWriteLine(
            stdout,
            fgCyan,
            styleBright,
            &"""  + {key:<11}""",
            resetStyle,
            value
        )
    elif value.len() > val_width:
        var value_list = newSeq[string]()
        var i = 0
        while i < value.len() - 1:
            try:
                let end_i = i + val_width
                let slice = value[i .. end_i]
                value_list.add(slice)
                inc(i, val_width + 1)
            except IndexError:
                let slice = value[i .. value.len() - 1]
                value_list.add(slice)
                inc(i, val_width + 1)
        styledWriteLine(
            stdout,
            fgCyan,
            styleBright,
            &"""  + {key:<11}""",
            resetStyle,
            value_list[0]
        )
        var j = 1
        while j < value_list.len():
            styledWriteLine(
                stdout,
                "               " & value_list[j]
            )
            inc(j)


proc writeLines(query_type: string, query_term: string, rsp_body: string) = 
    if query_type == "ip_addr":
        infoWrite("IP Address:", query_term)
    elif query_type == "cidr":
        infoWrite("CIDR Range:", query_term)
    elif query_type == "asn":
        infoWrite("ASN:", query_term)
    elif query_type == "domain":
        infoWrite("Domain:", query_term)
    let json_body = parseJson(rsp_body)
    # start address
    let start_address = json_body{"startAddress"}.getStr
    # end address
    let end_address = json_body{"endAddress"}.getStr
    # network
    if start_address != "" and end_address != "":
        let network = start_address & " - " & end_address
        infoWrite("Network:", network)
    # ldhName
    let ldh_name = json_body{"ldhName"}
    if not ldh_name.isNil():
        infoWrite("LDH Name:", ldh_name.getStr)
    # source
    let port_43_whois = json_body{"port43"}.getStr
    if port_43_whois != "":
        case port_43_whois
        of "whois.arin.net":
            infoWrite(
                "Source:", 
                "American Registry for Internet Numbers (ARIN)"
            )
        of "whois.ripe.net":
            infoWrite(
                "Source:", 
                "Réseaux IP Européens Network Coordination Centre (RIPE)"
            )
        of "whois.apnic.net":
            infoWrite(
                "Source:", 
                "Asia Pacific Network Information Centre (APNIC)"
            )
        of "whois.afrinic.net":
            infoWrite(
                "Source:", 
                "African Network Information Centre (AFRINIC)"
            )
        of "whois.lacnic.net":
            infoWrite(
                "Source:", 
                "Latin America and Caribbean Network Information Centre (LACNIC)"
            )
        infoWrite("Port 43 Whois:", port_43_whois)
    # cidr ranges
    var cidr_ranges = newSeq[string]()
    if "cidr0_cidrs" in json_body:
        for i in json_body["cidr0_cidrs"]:
            if "length" in i:
                if "v6prefix" in i:
                    let cidr = join(
                        [
                            i["v6prefix"].getStr, 
                            $i["length"].getInt
                        ], "/"
                    )
                    cidr_ranges.add(cidr)
                elif "v4prefix" in i:
                    let cidr = join(
                        [
                            i["v4prefix"].getStr, 
                            $i["length"].getInt
                        ], "/"
                    )
                    cidr_ranges.add(cidr)
        if (cidr_ranges.len() == 1) and (query_type != "cidr"):
            infoWrite("CIDR Range:", cidr_ranges[0])
        elif cidr_ranges.len() > 1:
            # tested with 221.1.1.1
            infoWrite("CIDR Ranges:", "")
            for i in cidr_ranges:
                bulletWrite(i)
    # net name
    let net_name = json_body{"name"}.getStr
    if net_name != "":
        infoWrite("Net Name:", net_name)
    # handle
    let handle = json_body{"handle"}.getStr
    if handle != "":
        infoWrite("Handle:", handle)
    # parent handle
    let parent_handle = json_body{"parentHandle"}.getStr
    if parent_handle != "":
        infoWrite("Parent Handle:", parent_handle)
    # net type
    let net_type = json_body{"type"}.getStr
    if net_type != "":
        infoWrite("Net Type:", net_type)
    # country
    let country = json_body{"country"}.getStr
    if country != "":
        infoWrite("Country:", country)
    # origin as
    let origin_as = json_body{"arin_originas0_originautnums"}
    if origin_as.getStr != "":
        if origin_as.len() == 1:
            infoWrite("Origin AS:", $origin_as[0].getInt)
        elif origin_as.len() > 1:
            infoWrite("Origin AS:", "")
            for i in origin_as:
                bulletWrite($i.getInt)
    # event dates
    let events = json_body{"events"}
    if not events.isNil:
        for i in events:
            let event_action = i{"eventAction"}.getStr & ": "
            let event_date = i{"eventDate"}.getStr
            if event_action != "" and event_date != "":
                var formatted_words = newSeq[string]()
                let action_words = event_action.split(" ")
                for word in action_words:
                    formatted_words.add(capitalizeAscii(word))
                let formatted_event = join(formatted_words, " ")
                if formatted_event.len > 14:
                    infoWrite(formatted_event, "")
                    bulletWrite(event_date)
                else:
                    infoWrite(formatted_event, event_date)
    # status
    let status = json_body{"status"}
    if not status.isNil:
        infoWrite("Status:", "")
        for i in status:
            bulletWrite(i.getStr)
    # remarks
    let remarks = json_body{"remarks"}
    if not remarks.isNil:
        for i in remarks:
            let title = i{"title"}
            let description = i{"description"}
            if not title.isNil and not description.isNil:
                let formatted_title = capitalizeAscii(title.getStr) & ":"
                var desc_elements = newSeq[string]()
                for j in description:
                    desc_elements.add(j.getStr)
                if desc_elements.len() == 1:
                    if formatted_title.len() > 14:
                        infoWrite(formatted_title, "")
                        bulletWrite(desc_elements[0])
                    else:
                        infoWrite(formatted_title, desc_elements[0])
                elif desc_elements.len() > 1:
                    infoWrite(formatted_title, "")
                    for ii in desc_elements:
                        if strip(ii) != "":
                            bulletWrite(ii)
    # links
    let links = json_body{"links"}
    if not links.isNil:
        infoWrite("Links:", "")
        for i in links:
            let rel = i{"rel"}.getStr
            let href = i{"href"}.getStr
            if rel != "" and href != "":
                let formatted_rel = capitalizeAscii(rel & ":")
                let formatted_href = href
                infoBulletWrite(formatted_rel, formatted_href)
    # nameservers
    let nameservers = json_body{"nameservers"}
    if not nameservers.isNil:
        if nameservers.len() >= 1:
            infoWrite("Nameservers:", "")
            for i in nameservers:
                let ns = i{"ldhName"}
                if not ns.isNil:
                    bulletWrite(ns.getStr)
    # secure dns
    let secure_dns = json_body{"secureDNS"}
    if not secure_dns.isNil:
        # signing
        let zone_signed = secure_dns{"zoneSigned"}
        let delegation_signed = secure_dns{"delegationSigned"}
        if not zone_signed.isNil or not delegation_signed.isNil:
            infoWrite("DNS Signed:", "")
        if not zone_signed.isNil:
            infoBulletWrite("Zone:", $zone_signed.getBool)
        if not delegation_signed.isNil:
            infoBulletWrite("Delegated:", $delegation_signed.getBool)
        # siglife
        let sig_life = secure_dns{"maxSigLife"}
        if not sig_life.isNil:
            infoWrite("Sig Life:", $sig_life.getInt)
        # dsData
        let ds_data = secure_dns{"dsData"}
        if not ds_data.isNil:
            infoWrite("DNS DS Count:", $ds_data.len())
            var ds_iter = 1
            for i in ds_data:
                infoWrite("DNS DS Record " & $ds_iter & ":", "")
                let key_tag = i{"keyTag"}
                if not key_tag.isNil:
                    infoBulletWrite("Key Tag:", $key_tag.getInt)
                let algorithm = i{"algorithm"}
                if not algorithm.isNil:
                    infoBulletWrite("Algorithm:", $algorithm.getInt)
                let digest = i{"digest"}
                if not digest.isNil:
                    infoBulletWrite("Digest:", $digest.getStr)
                let digest_type = i{"digestType"}
                if not digest_type.isNil:
                    infoBulletWrite("Digest Type:", $digest_type.getInt)
                inc(ds_iter)
        # keyData
        let key_data = secure_dns{"keyData"}
        if not key_data.isNil:
            infoWrite("DNS Key Count:", $key_data.len())
            var key_iter = 1
            for i in key_data:
                infoWrite("DNS Key Record " & $key_iter & ":", "")
                let flags = i{"flags"}
                if not flags.isNil:
                    infoBulletWrite("Flags:", $flags.getInt)
                let protocol = i{"protocol"}
                if not protocol.isNil:
                    infoBulletWrite("Protocol:", $protocol.getInt)
                let public_key = i{"publicKey"}
                if not public_key.isNil:
                    infoBulletWrite("Public Key:", $public_key.getStr)
                let algorithm = i{"algorithm"}
                if not algorithm.isNil:
                    infoBulletWrite("Algorithm:", $algorithm.getInt)
                inc(key_iter)
    # entities
    let entities = json_body{"entities"}
    if not entities.isNil():
        let entity_ct = entities.len()
        infoWrite("Entity Count:", $entity_ct)
        var no = 1
        for i in entities:
            let entity_no = "Entity " & $no & ":"
            infoWrite(entity_no, "")
            inc(no)
            # handle
            let handle = i{"handle"}
            if not handle.isNil:
                infoBulletWrite("Handle:", handle.getStr)
            # roles
            let roles = i{"roles"}
            if not roles.isNil:
                var roles_list = newSeq[string]()
                for j in roles:
                    roles_list.add(capitalizeAscii(j.getStr))
                let roles_str = join(roles_list, ", ")
                infoBulletWrite("Roles:", roles_str)
            # vcard
            let vcard_array = i{"vcardArray"}
            if not vcard_array.isNil():
                if vcard_array.len() == 2:
                    for j in vcard_array[1]:
                        if j.len() == 4:
                            var key: string
                            let key_a = j[0].getStr 
                            case key_a
                            of "fn":
                                key = "Full Name:"
                            of "email":
                                key = "Email:"
                            of "kind":
                                key = "Kind:"
                            of "adr":
                                key = "Address:"
                            of "tel":
                                key = "Telephone:"
                            of "org":
                                key = "Org:"
                            of "version":
                                key = "Version"
                            else:
                                key = key_a & ":"
                            var value_a: string
                            var value_b: string
                            var value: string
                            try:
                                let value_seq = to(j[3], seq[string])
                                value_a = strip(join(value_seq))
                            except JsonKindError:
                                value_a = j[3].getStr
                            if "label" in j[1] and value_a == "":
                                value_b = j[1]["label"].getStr
                                value = replace(replace(value_b, "\n", " "), "\r", " ")
                            else:
                                value = replace(replace(value_a, "\n", " "), "\r", "")
                            infoBulletWrite(key, value)

